<?php
namespace Action\View;

require_once "./modules/images.php";
require_once "./modules/students.php";

function init($db, $cfg) {
    $id = isset($_GET["id"]) ? intval($_GET["id"]) : null;

    $image = \Images\select($db, $id);
    if (!empty($image)) {
        $image["students"] = \Students\selectByImg($db, $image["id"]);
    }
    $imageNav = \Images\getNeighbors($db, $image);

    return array(
        'image'  => $image,
        'btn'  => $imageNav
    );
}
