<?php
namespace Action\Login;

require_once "./modules/user.php";

function init($db, $cfg) {
    if (isset($_SESSION["user"])) {
        return array(
            "redirect" => "./index.php?page=list"
        );
    }
}

function login($db, $cfg) {
    $name = $_POST["username"];
    $pass = $_POST["password"];
    \User\login($db, $name, $pass);
    return array(
        "redirect" => "./index.php?page=home"
    );
}

function logout($db, $cfg) {
    \User\logout();
    return array(
        "redirect" => "./index.php?page=home"
    );
}
