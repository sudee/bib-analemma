<?php
namespace Action\PhotoList;

require_once "./modules/images.php";
require_once "./modules/students.php";
require_once "./modules/utils.php";
require_once "./modules/json.php";

function init($db, $cfg) {
    $images = \Images\select($db);

    foreach ($images as $k => $v) {
        $images[$k]["students"] = \Students\selectByImg($db, $images[$k]["id"]);
    }

    return array(
        'images' => array_map(function ($x) { return \Images\format($x, "Y-m-d"); }, $images)
    );
}

function getAllCoord($db, $cfg) {
    return \Images\select($db);
}

function getStudentImageMap($db, $cfg) {
    $r = array();
    $imgs = \Images\selectAllByPicTaken($db);
    foreach($imgs as $image) {
        $r[$image["sid"]] []= $image["iid"];
    }
    return $r;
}

function upload($db, $cfg) {
    $uploads = \Utils\transpose($_FILES["files"]);
    $images = \Images\select($db);

    foreach ($uploads as $file) {
        if ($file["error"] === 0 && \Images\isJpegImage($file["tmp_name"])) {
            move_uploaded_file($file["tmp_name"], $cfg["res"]["dir"]["IMAGES"] . "/" . $file["name"]);
            $img = \Images\create($file["name"]);
            $images = \Images\insert($db, $img);
        }
    }

    return array(
        "redirect" => "./index.php?page=list"
    );
}
