<?php
namespace Action\Home;

require_once "./modules/images.php";
require_once "./modules/students.php";
require_once "./modules/json.php";

function init($db, $cfg) {
    $images = \Images\select($db);
    $img = end($images);
    $img["students"] = \Students\selectByImg($db, $img["id"]);

    $stat = array(
        'total' => empty($images) ? 1 : count($images),
        'visible' => count(array_filter($images, function ($img) { return $img["is_visible"]; }))
    );

    $schedule = \Students\selectBySchedule($db);
    $names = \Students\selectAllByPicTaken($db);

    return array(
        'stat' => $stat,
        'schedule' => $schedule,
        'img' => \Images\format($img),
        'names' => $names
    );
}
