<?php
namespace Action\Photo;

require_once "./modules/images.php";
require_once "./modules/students.php";
require_once "./modules/json.php";

function init($db, $cfg) {
    $id = isset($_GET["id"]) ? intval($_GET["id"]) : null;

    $image = \Images\select($db, $id);
    if (!empty($image)) {
        $image["students"] = \Students\selectByImg($db, $image["id"]);
    }
    $imageNav = \Images\getNeighbors($db, $image);

    return array(
        'image'  => $image,
        'btn'  => $imageNav
    );
}

function setCoord($db, $cfg) {
    $id = $_POST["id"];
    $img = \Images\select($db, $id);
    if ($img !== null) {
        $img["pos"] =  $_POST["sunPos"];
        $img["is_visible"] = json_decode($_POST["is_visible"]);
        \Images\updatePos($db, $id, $img);
        return array("status" => 0);
    } else {
        return array(
            "status" => 1,
            "error" => "Image not found."
        );
    }
}

function setValidity($db, $cfg) {
    $id = $_POST["id"];
    $img = \Images\select($db, $id);
    if ($img !== null) {
        $img["is_valid"] = json_decode($_POST["is_valid"]);
        \Images\updateValidity($db, $id, $img);
        return array("status" => 0);
    } else {
        return array(
            "status" => 1,
            "error" => "Image not found."
        );
    }
}
