<?php
namespace Action\Schedule;

require_once "./modules/utils.php";
require_once "./modules/images.php";
require_once "./modules/students.php";
require_once "./modules/json.php";

function init($db, $cfg) {
    $monthNames = array('Január', 'Február', 'Március', 'Április',
                        'Május', 'Június', 'Július', 'Augusztus',
                        'Szeptember', 'Október', 'November', 'December');
    $month = 'all';
    $images = array();
    $tmp = intval(\Utils\reqGET("month"));
    if (\Utils\inRange($tmp, 1, 12)) $month = $tmp;

    if ($month === 'all') {
        $images = \Images\select($db);

        foreach ($images as $k => $v) {
            $images[$k]["students"] = \Students\selectByImg($db, $images[$k]["id"]);
        }

    } else {
        $images = array();
        $start = new \DateTime("2015-01-01 +$month months -1 months");
        $end = new \DateTime("2015-01-01 +$month months");
        $interval = new \DateInterval('P1D');
        $dates = new \DatePeriod($start, $interval, $end);
        $photos = \Images\selectByMonth($db, $month);
        $pairs = \Students\selectByDatePeriod($db, $dates);

        $pairs = array_map(function ($xs) {
            return array_map(function ($x) { return $x['name']; }, $xs);
        }, $pairs);

        foreach($dates as $date) {
            $img = current($photos);
            $i = 0;
            while ($img['date'] < $date->format("c") && $i < 100) {
                $img = next($photos);
                $i++;
            }
            $match = (substr($img['date'], 0, 10) === $date->format("Y-m-d"));
            $images []= array(
                "date" => $date->format("Y-m-d"),
                "students" => implode(' - ', current($pairs)),
                "is_visible" => $match ? $img['is_visible'] : null,
                "id" => $match ? $img['id'] : null
            );
            next($pairs);
        }
    }

    return array(
        'images' => $images,
        'month' => $month,
        'months' => $monthNames
    );
}
