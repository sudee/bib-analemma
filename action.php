<?php
require_once "./modules/utils.php";
require_once "./modules/json.php";
require_once "./modules/datasource.php";

$config = \Utils\getConfig();
\Utils\sessionInit($config);

$db = \Datasource\getDatabase();
$actions = $config["actions"];

$cmd = \Utils\reqPOST("cmd");

if ($cmd && array_key_exists($cmd, $actions)) {
    $responseData = \Utils\execute($cmd, $db, $config);
    if (is_array($responseData) && array_key_exists("redirect", $responseData)) {
        \Utils\redirect($responseData["redirect"]);
    } else {
        \JSON\response($responseData);
    }
} else {
    \JSON\response(array(
        "status" => 1,
        "error" => "Requested action does not exist."
    ));
}
