define(['consts', 'events', 'jquery'], function (Consts, Events, $) {

    $(function () {
        $(Consts.CANVAS).on("click", Events.PosUpdateHandler);
        $(".custom-radio input[type=radio]").on("click", Events.PosUpdateHandler);
        $(window).on("resize", Events.AdjustCanvasSize);
        $("#validity").on("click", Events.ValidityUpdateHandler);
        Events.AdjustCanvasSize();
        $(Consts.CANVAS).css("visibility", "visible");
    });
});
