define(['consts'], function (Consts) {

    var circleColor = 'rgba(255, 255, 255, 0.4)';
    var studentMap;

    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    Point.prototype.toString = function () {
        return this.x + ";" + this.y;
    }

    function getImageRatio() {
        var original = 2048, resized = $(Consts.IMAGE).width();
        return original / resized;
    }

    function isValidImage(r) {
        var ms = 5 * 60000;
        var dd = new Date(r.date);
        var dn = new Date(r.date.substr(0, 10) + " 12:00:00");
        return r.pos !== null
            && (r.is_valid === "1" && r.is_visible === "1")
            // && Math.abs(dd - dn) < ms
        ;
    }

    function transformImage(r) {
        var pos = r.pos.split(";").map(function (x) {
            return parseInt(x, 10);
        });
        r.pos = new Point(pos[0], pos[1]);
        return r;
    }

    function realCoords(p) {
        var R = getImageRatio();
        return new Point(
            Math.round(p.x * R),
            Math.round(p.y * R)
        );
    }

    function screenCoords(p) {
        var R = getImageRatio();
        return new Point(
            Math.round(p.x / R),
            Math.round(p.y / R)
        );
    }

    function makeRequest(data) {
        return $.ajax({
            method: "POST",
            url: "action.php",
            data: data
        });
    }

    return {
        "Point": Point,
        "isValidImage": isValidImage,
        "transformImage": transformImage,
        "makeRequest": makeRequest,
        "realCoords": realCoords,
        "screenCoords": screenCoords
    };
});
