requirejs.config({
    baseUrl: 'assets/script',
    paths: {
        jquery: 'lib/jquery-2.1.3.min',
        lodash: 'lib/lodash.min'
    }
});
