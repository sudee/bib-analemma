define(['consts', 'utils', 'display-methods', 'data', 'lodash', 'jquery'],
    function (Consts, Utils, DisplayMethods, Data, _, $) {

    var pointRadius = 40;
    var ctx = $(Consts.CANVAS)[0].getContext("2d");

    function getImgPos() {
        var p = $("#img-pos").val();
        if (p) {
            p = p.split(";").map(function (x) {
                return parseInt(x, 10);
            });
            return new Utils.Point(p[0], p[1]);
        }
        return "";
    }

    function DisplayModeUpdateHandler(evt) {
        var i, f, names;
        DisplayMethods.reset(ctx);
        names = _.keys(DisplayMethods.methods);

        i = window.parseInt($(Consts.DISP_MOD).val(), 10);
        $(Consts.STUD_SEL)[(i == 0) ? "show" : "hide"]();

        f = names[i < names.length ? i : 0];
        Data.loadImages(function (imageData) {
            DisplayMethods.methods[f](ctx, imageData);
        });
    }

    function PosUpdateHandler(evt) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        var x, y, pos;
        var isVisible = (evt.target.id != "visibleBtn2");
        var visCtrl = !!$(evt.target).closest(".custom-radio").length;
        if (visCtrl) {
            pos = getImgPos();
        } else {
            x = evt.pageX - $(Consts.CANVAS).offset().left;
            y = evt.pageY - $(Consts.CANVAS).offset().top;
            pos = Utils.realCoords(new Utils.Point(x, y));
        }

        Utils.makeRequest({
            "cmd": "set-coord",
            "id": $("#img-id").val(),
            "is_visible": isVisible,
            "sunPos": pos.toString()
        }).done(function(data) {
            if (isVisible) {
                $("#pos").text(pos.toString());
                $("#img-pos").val(pos.toString());
                DisplayMethods.drawCircle(ctx, Utils.screenCoords(pos), pointRadius);
            }
        });
    }

    function ValidityUpdateHandler(evt) {
        Utils.makeRequest({
            "cmd": "set-valid",
            "id": $("#img-id").val(),
            "is_valid": $(this).is(":checked")
        });
    }

    function WindowResizeHandler() {
        $(Consts.CANVAS).attr("width", $(Consts.IMAGE).width());
        $(Consts.CANVAS).attr("height", $(Consts.IMAGE).height());

        var pos = getImgPos();
        if (pos) {
            DisplayMethods.drawSunPos(ctx, pos);
        }
    }

    return {
        "DisplayModeUpdateHandler": DisplayModeUpdateHandler,
        "PosUpdateHandler": PosUpdateHandler,
        "ValidityUpdateHandler": ValidityUpdateHandler,
        "AdjustCanvasSize": WindowResizeHandler
    };
});
