define(['consts', 'utils', 'data', 'jquery'], function (Consts, Utils, Data, $) {

    var circleColor = 'rgba(255, 255, 255, 0.4)';
    // var circleColor = 'rgba(0, 0, 255, 0.4)';
    var circleColorSel = 'rgba(0, 0, 255, 0.8)';
    var circleColorAdmin = 'rgba(255, 0, 0, 0.4)';
    var iid;

    function getPosList(imageData) {
        return imageData.map(function (r) {
            return r.pos;
        })
    };

    function reset(ctx) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        window.clearInterval(iid);
        $(Consts.CANVAS).off("click.drawCircle");
    }

    function drawCircle(ctx, p, r) {
        ctx.beginPath();
        ctx.arc(p.x, p.y, r, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.stroke();
    }

    function drawAllCircles(ctx, posList) {
        ctx.fillStyle = circleColor;
        ctx.strokeStyle = 'rgba(0,0,0,0.3)';
        posList.forEach(function (p, i) {
            drawCircle(ctx, Utils.screenCoords(p), 5);
        });
    }

    function drawSelectedCircles(ctx, imageData, selected) {
        Data.loadStudentMap(function (data) {
            imageData.forEach(function (p, i) {
                if (data[selected].indexOf(p.id) !== -1) {
                    ctx.fillStyle = circleColorSel;
                    drawCircle(ctx, Utils.screenCoords(p.pos), 10);
                } else {
                    ctx.fillStyle = circleColor;
                    drawCircle(ctx, Utils.screenCoords(p.pos), 5);
                }
            });
        });
    }

    function drawCircles(ctx, imageData) {
        var sv = $(Consts.STUD_VAL).val();
        if (sv == "all") {
            drawAllCircles(ctx, getPosList(imageData));
        } else {
            drawSelectedCircles(ctx, imageData, sv);
        }
    }

    function drawPath(ctx, imageData) {
        var posList = getPosList(imageData);
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#0088FF';
        ctx.beginPath();
        posList.forEach(function (p, i) {
            p = Utils.screenCoords(p);
            ctx.lineTo(p.x, p.y);
        });
        ctx.stroke();
    }

    function animateCircles(ctx, imageData) {
        var posList = getPosList(imageData);
        ctx.fillStyle = circleColor;
        var i = 0;
        iid = window.setInterval(function () {
            drawCircle(ctx, Utils.screenCoords(posList[i]), 5);
            ctx.closePath();
            i++;
            if (i == posList.length) window.clearInterval(iid);
        }, 800);
    }

    function drawOnClick(ctx, imageData) {
        var posList = getPosList(imageData);
        var i = 0;
        ctx.fillStyle = circleColor;
        $(Consts.CANVAS).on("click.drawCircle", function (evt) {
            if (i < posList.length) {
                drawCircle(ctx, Utils.screenCoords(posList[i]), 5);
                i++;
            }
        });
    }

    function drawSunPos(ctx, pos) {
        var pointRadius = 40;
        ctx.fillStyle = circleColorAdmin;
        drawCircle(ctx, Utils.screenCoords(pos), pointRadius);
    }


    return {
        "reset": reset,
        "drawCircle": drawCircle,
        "drawSunPos": drawSunPos,
        "methods": {
            "drawCircles": drawCircles,
            "drawPath": drawPath,
            "animateCircles": animateCircles,
            "drawOnClick": drawOnClick
        }
    };
});
