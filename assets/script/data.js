define(['consts', 'utils', 'jquery'], function (Consts, Utils, $) {

    var imageData, studentMap;

    function errorHandler() {
        console.trace("Unexpected error.");
    }

    function useImageData(callback) {
        if (imageData) {
            callback.call(null, imageData);
            return;
        }
        Utils.makeRequest({
            cmd: "get-coord"
        }).done(function(data) {
            imageData = data
                .filter(Utils.isValidImage)
                .map(Utils.transformImage);
            callback.call(null, imageData);
        }).fail(errorHandler);
    }

    function useStudentMap(callback) {
        if (studentMap) {
            callback.call(null, studentMap);
            return;
        }
        Utils.makeRequest({
            cmd: "get-studs"
        }).done(function(data) {
            studentMap = data;
            callback.call(null, studentMap);
        }).fail(errorHandler);
    }


    return {
        "loadStudentMap": useStudentMap,
        "loadImages": useImageData
    };
});
