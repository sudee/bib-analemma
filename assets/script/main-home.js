define(['consts', 'events', 'data', 'jquery'], function (Consts, Events, Data, $) {

    $(function () {
        $(Consts.DISP_MOD).on("change", Events.DisplayModeUpdateHandler);
        $(Consts.STUD_VAL).on("change", Events.DisplayModeUpdateHandler);
        $(window).on("resize", Events.AdjustCanvasSize);
        $(window).on("resize", Events.DisplayModeUpdateHandler);
        Events.AdjustCanvasSize();
        Events.DisplayModeUpdateHandler();
        $(Consts.CANVAS).css("visibility", "visible");
    });
});
