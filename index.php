<?php
require_once "./modules/utils.php";
require_once "./modules/json.php";
require_once "./modules/datasource.php";

$config = \Utils\getConfig();
\Utils\sessionInit($config);

$pagelist = $config["pages"];
$db = \Datasource\getDatabase();

$pagename = array_key_exists("page", $_GET) ? $_GET["page"] : "home";
$pagename = array_key_exists($pagename, $pagelist) ? $pagename : "home";

if (empty($_SESSION["user"]) && $pagelist[$pagename]["access"] !== 0) {
    \Utils\redirect('./index.php?page=login');
}

\Utils\render($pagename, $db, $config);
