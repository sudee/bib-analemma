<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><?php echo $p["title"]; ?></title>
        <link rel="stylesheet" type="text/css" href="assets/style/base.css" />
        <link rel="stylesheet" type="text/css" href="assets/style/style.css" />
        <?php if ($p["js-main"] !== null): ?>
        <script data-main="assets/script/<?php echo $p["js-main"]; ?>" src="assets/script/lib/require.js"></script>
        <?php endif ?>
    </head>
    <body>
<?php require "./templates/partial/nav.php"; ?>
