<?php if (empty($v["image"])) { ?>
        <h1>Érvénytelen azonosító!</h1>
<?php } else { ?>
        <div id="container1">
            <div id="img-target">
                <img id="img1" src="assets/images/<?php echo $v["image"]["name"]; ?>" alt="<?php echo $v["image"]["name"]; ?>">
            </div>
            <canvas id="canvas1" class="noselect"></canvas>
            <input id="img-id" type="hidden" value="<?php echo $v["image"]["id"]; ?>" />
            <input id="img-pos" type="hidden" value="<?php echo $v["image"]["pos"]; ?>" />
            <input id="img-src" type="hidden" value="<?php echo $v["image"]["name"]; ?>" />
        </div>
        <div id="img-data">
            <div>
                <?php if ($v["btn"]["prev"]): ?>
                <a href="?page=photo&amp;id=<?php echo $v["btn"]["prev"]; ?>"> &lt;&lt; Előző kép</a>
                <?php endif; ?>
                <?php if ($v["btn"]["next"]): ?>
                <a href="?page=photo&amp;id=<?php echo $v["btn"]["next"]; ?>">Következő kép &gt;&gt; </a>
                <?php endif; ?>
            </div>
            <div>Azonosító: <?php echo $v["image"]["id"]; ?></div>
            <div>Kép: <?php echo $v["image"]["name"]; ?></div>
            <div>Dátum: <?php echo $v["image"]["date"]; ?></div>
            <div>Készítette: <?php echo $v["image"]["students"]; ?></div>
            <div class="custom-radio clearfix">
                <div>
                    <input id="visibleBtn1" type="radio" name="visible" <?php if ($v["image"]["is_visible"]) echo "checked"; ?>>
                    <label for="visibleBtn1"><span>A Nap látható: <span id="pos"><?php echo $v["image"]["pos"]; ?></span></span></label>
                </div>
                <div>
                    <input id="visibleBtn2" class=".not-visible" type="radio" name="visible" <?php if (!$v["image"]["is_visible"]) echo "checked"; ?>>
                    <label for="visibleBtn2"><span>A Nap nem látható</span></label>
                </div>
            </div>
            <div>Érvényes-e: <input id="validity" type="checkbox" <?php echo $v["image"]["is_valid"] ? "checked" : ""; ?>></div>
        </div>
        <div class="clear"></div>
<?php } ?>
