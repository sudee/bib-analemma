<?php if (empty($v["image"])) { ?>
        <h1>Érvénytelen azonosító!</h1>
<?php } else { ?>
        <div id="container1">
            <div id="img-target">
                <img id="img1" src="assets/images/<?php echo $v["image"]["name"]; ?>" alt="<?php echo $v["image"]["name"]; ?>">
            </div>
            <!-- <canvas id="canvas1" class="noselect"></canvas> -->
            <input id="img-id" type="hidden" value="<?php echo $v["image"]["id"]; ?>" />
            <input id="img-pos" type="hidden" value="<?php echo $v["image"]["pos"]; ?>" />
            <input id="img-src" type="hidden" value="<?php echo $v["image"]["name"]; ?>" />
        </div>
        <div id="img-data">
            <div>
                <?php if ($v["btn"]["prev"]): ?>
                <a href="?page=view&amp;id=<?php echo $v["btn"]["prev"]; ?>"> &lt;&lt; Előző kép</a>
                <?php endif; ?>
                <?php if ($v["btn"]["next"]): ?>
                <a href="?page=view&amp;id=<?php echo $v["btn"]["next"]; ?>">Következő kép &gt;&gt; </a>
                <?php endif; ?>
            </div>
            <div>Azonosító: <?php echo $v["image"]["id"]; ?></div>
            <div>Kép: <?php echo $v["image"]["name"]; ?></div>
            <div>Dátum: <?php echo $v["image"]["date"]; ?></div>
            <div>Készítette: <?php echo $v["image"]["students"]; ?></div>
            <div>A Nap látható: <?php echo $v["image"]["is_visible"]
                ? "Igen (" . ($v["image"]["pos"] ?: "?") . ")"
                : "Nem"; ?></div>
        </div>
        <div class="clear"></div>
<?php } ?>
