<div class="months">
    <a href="?page=sched&amp;month=all"
        <?php if (!\Utils\inRange($v["month"], 1, 12)) echo ' class="active"'; ?>>Összes</a>
    <?php for ($i = 1; $i <= 12; $i++): ?>
    <a href="?page=sched&amp;month=<?php echo $i; ?>"
        <?php if ($v["month"] == $i) echo ' class="active"'; ?>><?php echo $v["months"][$i - 1]; ?></a>
    <?php endfor; ?>
</div>
<table class="image-table schedule-table">
    <thead>
        <tr>
            <th>Dátum</th>
            <th>Készítette</th>
            <th>Nap látható?</th>
            <th>Kép</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($v["images"] as $image) { ?>
        <tr>
            <td><?php echo substr($image["date"], 0, 10); ?></td>
            <td><?php echo $image["students"]; ?></td>
            <td><?php echo \Images\getSunStr($image); ?></td>
            <td>
                <?php if (isset($image["id"])): ?>
                <a href="?page=view&amp;id=<?php echo $image["id"]; ?>">Megnyitás</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
