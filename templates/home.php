        <div id="container1">
            <div id="img-target">
                <img id="img1" src="assets/images/<?php echo $v["img"]["name"]; ?>" alt="<?php echo $v["img"]["name"]; ?>">
            </div>
            <canvas id="canvas1" class="noselect"></canvas>
        </div>
        <div id="img-data">
            <h4>Adatok:</h4>
            <div>
                <div>Feltöltött képek: <?php echo $v["stat"]["total"]; ?></div>
                <div>Nap látható: <?php echo sprintf("%d képen (%.2f%%)", $v["stat"]["visible"], 100 * $v["stat"]["visible"] / $v["stat"]["total"]); ?></div>
            </div>
            <h4>Legutóbbi kép adatai:</h4>
            <div>
                <div>Dátum: <?php echo $v["img"]["date"]; ?></div>
                <div>Készítette: <?php echo $v["img"]["students"]; ?></div>
                <div>Nap: <?php echo $v["img"]["is_visible"]; ?></div>
            </div>
            <input id="img-src" type="hidden" value="<?php echo $v["img"]["name"]; ?>" />
            <div>
                <h4>Megjelenítési mód:</h4>
                <select id="display-mode">
                    <option value="0" selected>Pontok kirajzolása</option>
                    <!-- <option value="1">Vonal kirajzolása</option> -->
                    <option value="2">Diavetítés</option>
                    <option value="3">Léptetés</option>
                </select>
            </div>
            <div id="student-selector">
                <h4>Tanulók:</h4>
                <select id="student-select">
                    <option value="all" selected> -- Minden tanuló -- </option>
                <?php foreach ($v["names"] as $name): ?>
                    <option value="<?php echo $name["id"]; ?>"><?php echo $name["name"]; ?></option>
                <?php endforeach; ?>
                </select>
            </div>
            <div>
                <h4>Következő 7 nap beosztása:</h4>
                <table>
                    <thead>
                        <tr>
                            <th>Név</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($v["schedule"] as $s): ?>
                        <tr>
                            <td><?php echo $s["name"]; ?></td>
                            <td><?php echo $s["start_date"], " – ", $s["end_date"]; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clear"></div>
