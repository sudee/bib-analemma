        <div id="upload-form">
            <form action="action.php" method="POST" enctype="multipart/form-data">
                <div>
                    <input id="img-input" type="file" name="files[]" multiple />
                </div>
                <div>
                    <input type="hidden" name="cmd" value="upload" />
                    <button type="submit">Feltöltés</button>
                </div>
            </form>
        </div>

        <table class="image-table">
            <thead>
                <tr>
                    <th>Azonosító</th>
                    <th>Kép</th>
                    <th>Dátum</th>
                    <th>Készítette</th>
                    <th>Nap látható?</th>
                    <th>Érvényes pozíció?</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($v["images"] as $image) { ?>
                <tr>
                    <td><?php echo $image["id"]; ?></td>
                    <td><?php echo $image["name"]; ?></td>
                    <td><?php echo $image["date"]; ?></td>
                    <td><?php echo $image["students"]; ?></td>
                    <td><?php echo $image["is_visible"]; ?></td>
                    <td<?php echo !$image["is_valid"] ? ' class="table-cell-false"' : "";?>><?php echo $image["is_valid"] ? "Igen" : "Nem"; ?></td>
                    <td class="action"><a href="?page=photo&amp;id=<?php echo $image["id"]; ?>">Szerkeszt</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
