<?php
namespace Utils;

require_once "./modules/json.php";

function render($pagename, $db, $config) {
    // general page variables
    $p = $config["pages"][$pagename];
    // template variables
    $v = \Utils\execute(array($pagename, "init"), $db, $config);

    unset($pagename, $db, $config);
    require "./templates/partial/header.php";
    require "./templates/" . $p["template"];
    require "./templates/partial/footer.php";
}

function execute($cmd, $db, $config) {
    list($page, $action) = is_array($cmd) ? $cmd : $config["actions"][$cmd];
    require_once "./actions/" . $page . ".php";
    return call_user_func(
        $config["pages"][$page]["action"] . "\\" . $action, $db, $config
    );
}

function getConfig() {
    return \JSON\open('./data/app.json');
}

function arrayGet($ary, $key, $default = null) {
    return array_key_exists($key, $ary) ? $ary[$key] : $default;
}

function reqGET($name) {
    return arrayGet($_GET, $name);
}

function reqPOST($name) {
    return arrayGet($_POST, $name);
}

function link($params) {
    // return http_build_query($params, '', '&amp;');
}

function sessionInit($cfg) {
    session_name($cfg["appId"]);
    session_start();
}

function redirect($url) {
    header('Location: ' . $url, true, 303);
    exit();
}




function transpose($vector) {
    $result = array();
    foreach($vector as $key1 => $value1)
        foreach($value1 as $key2 => $value2)
            $result[$key2][$key1] = $value2;
    return $result;
}

function inRange($i, $min, $max) {
    return $i >= $min && $i <= $max;
}

function dateInInterval($date, $start, $end) {
    $start = date_create($start);
    $end   = date_create($end);
    $date  = date_create($date);
    return $date >= $start && $date <= $end;
}
