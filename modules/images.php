<?php
namespace Images;

function insert($db, $record) {
    $sql = "INSERT INTO images (name, date) VALUES (?, ?)";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($record["name"], $record["date"]));
    $id = $db->lastInsertId();

    $sql = "INSERT INTO positions (image_id) VALUES (?)";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($id));
}

function select($db, $id = null) {
    $sql = "SELECT I.*, P.pos, P.is_visible, P.is_valid FROM images AS I
            JOIN positions AS P ON P.image_id = I.id";
    if (isset($id)) {
        $sql .= " WHERE I.id = ? ORDER BY I.date";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        return $stmt->fetch();
    } else {
        $sql .= " ORDER BY I.date";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}

function update($db, $id, $record) {
    $sql = "UPDATE images SET name = ?, date = ? WHERE id = ?";
    $stmt = $db->prepare($sql);
    return $stmt->execute(array($record["name"], $record["date"], $id));
}

function updatePos($db, $id, $record) {
    $sql = "UPDATE positions SET pos = ?, is_visible = ? WHERE image_id = ?";
    $stmt = $db->prepare($sql);
    return $stmt->execute(array($record["pos"], $record["is_visible"] ? 1 : 0, $id));
}

function updateValidity($db, $id, $record) {
    $sql = "UPDATE positions SET is_valid = ? WHERE image_id = ?";
    $stmt = $db->prepare($sql);
    return $stmt->execute(array($record["is_valid"] ? 1 : 0, $id));
}

function delete($db, $id) {
    $sql = "DELETE FROM images WHERE id = ?";
    $stmt = $db->prepare($sql);
    return $stmt->execute(array($id));
}

function selectByMonth($db, $month) {
    $d1 = new \DateTime("2015-01-01 +$month months -1 months");
    $d2 = new \DateTime("2015-01-01 +$month months");
    $sql = "SELECT I.*, P.pos, P.is_visible, P.is_valid
            FROM images AS I
            JOIN positions AS P ON P.image_id = I.id
            WHERE date(I.date) >= date(?)
                AND date(I.date) < date(?)
            ORDER BY I.date";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($d1->format("Y-m-d"), $d2->format("Y-m-d")));
    return $stmt->fetchAll();
}

function selectAllByPicTaken($db) {
    $sql = "SELECT I.id iid, S.id sid
            FROM images I
            LEFT JOIN students S
            ON date(S.start_date) <= date(I.date)
                AND date(S.end_date) >= date(I.date)
            ORDER BY S.name";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}


function getNeighbors($db, $img) {
    $sql = "SELECT (SELECT id FROM images
            WHERE date = (SELECT max(date) FROM images WHERE date < ?)
            ORDER BY date) `prev`, (SELECT id FROM images
            WHERE date = (SELECT min(date) FROM images WHERE date > ?)
            ORDER BY date) `next`";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($img["date"], $img["date"]));
    return $stmt->fetch();
}

function isJpegImage($filename) {
    return filesize($filename) > 11 && exif_imagetype($filename) === IMAGETYPE_JPEG;
}

function create($filename) {
    $cfg = \Utils\getConfig();
    $exif = exif_read_data($cfg["res"]["dir"]["IMAGES"] . "/$filename", "FILE");
    $date = null;
    if (array_key_exists("DateTimeOriginal", $exif))
        $date = date("c", strtotime($exif["DateTimeOriginal"]));

    return array(
        "name" => $filename,
        "date" => $date
    );
}

function getSunStr($img) {
    return !isset($img["is_visible"]) ? "&lt;Nincs adat&gt;"
        : ($img["is_visible"] ? "Látható" : "Nem látható");
}

function format($img, $dateformat = "Y-m-d H:i:s") {
    $img["date"] = date($dateformat, strtotime($img["date"]));
    $img["is_visible"] = getSunStr($img);
    return $img;
}
