<?php
namespace User;

require_once "./modules/json.php";
require_once "./modules/datasource.php";
require_once "./modules/utils.php";

function create($name) {
    return array(
        "name" => $name
    );
}

function currentUser() {
    return array_key_exists("user", $_SESSION)
        ? $_SESSION["user"]
        : null;
}

function selectUser($db, $username) {
    $sql = "SELECT * FROM users WHERE name = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($username));
    return $stmt->fetch();
}

function login($db, $name, $password) {
    $user = selectUser($db, $name);

    if (count($user) !== 0 && $user["password"] === hash("sha256", $password)) {
        $_SESSION["user"] = create($name);
        return true;
    }
    return false;
}

function logout() {
    session_destroy();
}
