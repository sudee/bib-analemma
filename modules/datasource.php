<?php
namespace Datasource;

function getDatabase() {
    $db = new \PDO("sqlite:./data/analemma.db");
    $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
    return $db;
}
