<?php
namespace Students;

function selectAll($db) {
    $sql = "SELECT *
            FROM students AS S
            ORDER BY S.name";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

function selectByImg($db, $img_id) {
    $sql = "SELECT S.name FROM students AS S
            JOIN images AS I ON I.id = ?
              AND date(S.start_date) <= date(I.date)
              AND date(S.end_date) >= date(I.date)
            ORDER BY I.date";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($img_id));
    $names = $stmt->fetchAll();
    return implode(" - ", array_map(function ($r) {
        return $r["name"];
    }, $names));
}

function selectByDate($db, $date) {
    $sql = "SELECT S.name
            FROM students AS S
            WHERE date(S.start_date) <= date(?)
                AND date(S.end_date) >= date(?)";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($date));
    return $stmt->fetchAll();
}

function selectBySchedule($db) {
    $sql = "SELECT *
            FROM students AS S
            WHERE S.end_date >= date('now')
                AND S.start_date <= date('now', '+7 day')
            ORDER BY S.start_date";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

function selectByDatePeriod($db, $period) {
    $pairs = array();
    $sql = "SELECT S.name
            FROM students AS S
            WHERE date(S.start_date) <= date(?)
                AND date(S.end_date) >= date(?)";
    $stmt = $db->prepare($sql);
    foreach ($period as $date) {
        $df = $date->format("Y-m-d");
        $stmt->execute(array($df, $df));
        $pairs[$df] = $stmt->fetchAll();
    }
    return $pairs;
}

function selectAllPairs($db) {
    $sql = "SELECT S1.start_date, S1.name, S2.name
            FROM (
                SELECT *
                FROM students
                WHERE start_date IS NOT NULL
                ORDER BY name DESC
            ) S1
            LEFT JOIN students S2
            ON S1.id != S2.id
                AND date(S1.start_date) = date(S2.start_date)
            GROUP BY S1.start_date
            ORDER BY S1.start_date";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

function selectAllByPicTaken($db) {
    $sql = "SELECT DISTINCT S.id, S.name
            FROM images I
            LEFT JOIN students S
            ON date(S.start_date) <= date(I.date)
                AND date(S.end_date) >= date(I.date)
            ORDER BY S.name";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}
