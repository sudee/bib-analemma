<?php
namespace JSON;

function open($filename) {
    return json_decode(file_get_contents($filename), true);
}

function write($filename, $data) {
    file_put_contents($filename, json_encode($data));
}

function response($data) {
    $str = json_encode($data);
    $err = json_last_error();

    header('Content-type: application/json');
    echo $err === JSON_ERROR_NONE
        ? $str
        : json_encode(array("error" => $err));
}
